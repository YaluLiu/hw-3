//===--- Link_test.c - All test cases are included here ---===//
#include <stdio.h>
#include <stdlib.h>

#include "LinkList.h"

static int success = 1;
static int fail    = 0;

void test_begin(void) 
{
    printf("begin the test----------------- \n");
}

void test_end(void) 
{
    printf("end the test------------------- \n");
}

void test_try(int isTrue, const char* msg) 
{
    if(isTrue == success) 
    {
        printf("%s passed the case !Success! \n",msg);
    }
    else
    {
        printf("%s failed to pass the normal case\n",msg);
    }
}

// ****** Common Test cases ********//

static void test_Link_new_free() 
{
  Link_t s = Link_new();
  test_try(NULL != s, "The func Link_new_free()");
  Link_free(s);
  return ;
}


static void test_all()
{
  test_begin();
  test_Link_new_free();
  test_end();
}

int main() {
  test_all();
  return 0;
}
