/*************************************************************************
    > File Name:    LinkList.c
    > Author: liuyalu
    > Mail: 13844098386@163.com 
    > Created Time: 2014年07月14日 星期一 08时21分56秒
 ************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <malloc.h>

#include "LinkList.h"


//Create a new Link
Link_t Link_new(void)
{
    Link_t head = (Link_t)malloc(sizeof(node));
    if(NULL == head)
    {
        return NULL;
    }
    memset(head,0,sizeof(struct node));
    head->next = NULL;
    return head;
}


//free the memory of the Link
void Link_free(Link_t oLink)
{
    if(oLink == NULL) 
    {
        printf("The List was NULL!\n");
        return ;
    }
    Link_t  temp;
    while(oLink->next)
    {
        temp = oLink->next;
        free(oLink->pcKey);
        memset(oLink,0,sizeof(oLink));
        free(oLink);
        oLink = temp;
    }
    return ;
}

// print the list
void Link_print(Link_t oLink)
{
    printf("the list is :");
    if(oLink == NULL)
    {
        printf("NULL \n");
        return ;
    }
    Link_t temp = oLink;
    while(temp->next)
    {
        temp = temp->next;
        printf("(%d,",temp->pvValue);
        printf("%s)",temp->pcKey);
      //  printf("bug print!\n");
    }
    printf("\n");
}


























